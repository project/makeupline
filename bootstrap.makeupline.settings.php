<?php

/**
 * MakeUp Line settings
 */
// MakeUp content types and fields
$conf['makeup']['types'] = array('homepage_fields_content_lines');
$conf['makeup']['line_fields'] = array('field_line');
$conf['makeup']['entityref_field'] = 'field_entity_teaser';
$conf['makeup']['line_style'] = 'field_line_style';
$conf['makeup']['line_title'] = 'field_line_title';
$conf['makeup']['block_title'] = 'field_line_title';
$conf['makeup']['titles'] = 'field_titles';
$conf['makeup']['css_prefix'] = 'makeup-';

// MakeUp displays
$conf['makeup']['display']['top_teaser'] = array(
	'other' => array(
		'view_mode' => 'full_media',
		'width_class' => 'col-md-12',
	),
);
$conf['makeup']['display']['3col_simple'] = array(
	'other' => array(
		'view_mode' => '4col_simple',
		'width_class' => 'col-md-4',
		'well' => TRUE,
	),
);
$conf['makeup']['display']['3col_media'] = array(
	'other' => array(
		'view_mode' => '4col_media',
		'width_class' => 'col-md-4',
	),
);
$conf['makeup']['display']['2col_media'] = array(
	'1' => array(
		'view_mode' => '8col_media',
		'width_class' => 'col-md-8',
	),
	'other' => array(
		'view_mode' => '4col_media_light',
		'width_class' => 'col-md-4',
	),
);
$conf['makeup']['display']['2col_media_inv'] = array(
	'2' => array(
		'view_mode' => '8col_media',
		'width_class' => 'col-md-8',
	),
	'other' => array(
		'view_mode' => '4col_media_light',
		'width_class' => 'col-md-4',
	),
);
$conf['makeup']['display']['3col_social'] = array(
	'1' => array(
		'special' => 'comments',
		'width_class' => 'col-md-4',
	),
	'2' => array(
		'special' => 'twitter',
		'width_class' => 'col-md-4',
	),
	'3' => array(
		'special' => 'facebook',
		'width_class' => 'col-md-4',
	),
);

// MakeUp Line view modes for Displays
$conf['makeup']['view_mode']['node'] = array(
	'4col_simple' => '4col Simple',
	'4col_media' => '4col Media',
	'4col_media_light' => '4col Media Light',
	'8col_media' => '8col Media',
	'full_media' => 'Full Media',
);

// MakeUp Line view modes for EntityRef Dialog
$conf['makeup']['view_mode']['entityref_dialog'] = 'ER Dialog';
